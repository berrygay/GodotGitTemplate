using Godot;
using System;

public class PlayerController : RigidBody
{
	public override void _Process(float delta)
	{
		var velocity = new Vector3
		{
			x = (Input.GetActionStrength("world_right") - Input.GetActionStrength("world_left")),
			z = (Input.GetActionStrength("world_down") - Input.GetActionStrength("world_up")),
		};
		
		AddCentralForce(velocity * 10);
	}
}
