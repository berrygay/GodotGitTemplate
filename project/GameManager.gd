extends Node

export var player_path : NodePath

export var end_background_path : NodePath

func _process(_delta):
	var end_background := (get_node(end_background_path) as Control)

	if (not end_background.visible):
		var player := (get_node(player_path) as Spatial)

		if (player and player.translation.y < -5.0):
			player.queue_free()

			if (end_background):
				end_background.show()

func on_game_over():
	get_tree().quit()
